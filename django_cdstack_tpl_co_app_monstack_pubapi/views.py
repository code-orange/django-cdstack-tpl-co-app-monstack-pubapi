from django.template import engines

import copy

from django_cdstack_deploy.django_cdstack_deploy.func import generate_config_static
from django_cdstack_tpl_appsrv_uwsgi.django_cdstack_tpl_appsrv_uwsgi.views import (
    handle as handle_appsrv_uwsgi,
)


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_co_app_monstack_pubapi/django_cdstack_tpl_co_app_monstack_pubapi"

    generate_config_static(zipfile_handler, template_opts, module_prefix)
    handle_appsrv_uwsgi(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    return True
